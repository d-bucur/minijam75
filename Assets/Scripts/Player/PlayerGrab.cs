using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class PlayerGrab : MonoBehaviour {
    public float grabForce;
    private LinkedList<GameObject> grabbedItems = new LinkedList<GameObject>();
    private Vector3 _lastPos;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.GetComponentInParent<Interactor>()) {
            grabbedItems.AddLast(other.transform.parent.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.GetComponentInParent<Interactor>()) {
            grabbedItems.Remove(other.transform.parent.gameObject);
        }
    }

    private void Update() {
        var delta = transform.position - _lastPos;
        _lastPos = transform.position;
        if (!Input.GetKey(KeyCode.LeftShift)) return;
        foreach (var item in grabbedItems) {
            // item.GetComponent<Rigidbody2D>().MovePosition(item.transform.position + delta);
            var force = transform.position - item.transform.position;
            item.GetComponent<Rigidbody2D>().AddForce(force * (grabForce * Time.deltaTime));
        }
    }
}
