using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGravityGun : MonoBehaviour
{
    public float forceMultiplier;
    private Rigidbody2D _active;

    void Update() {
        if (Input.GetMouseButtonUp(0)) {
            _active = null;
        }
        var mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        PushActiveTo(mouseWorldPos);
        int layerMask = 1 << 6;
        var hit = Physics2D.Raycast(mouseWorldPos, Vector2.zero, 1f, layerMask);
        if (!hit.collider) return;
        
        var interactor = hit.collider.GetComponentInParent<Interactor>();
        if (!interactor) return;
        if (Input.GetMouseButtonDown(0)) {
            _active = interactor.GetComponent<Rigidbody2D>();
        }
    }

    private void PushActiveTo(Vector3 mouseWorldPos) {
        if (_active == null) return;
        var force = mouseWorldPos - _active.transform.position;
        force *= SpeedManager.Instance.gravityForceMultiplier;
        _active.AddForce(force * forceMultiplier * Time.deltaTime);
    }
}
