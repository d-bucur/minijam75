using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float jumpForce;
    [SerializeField] private float acceleration;
    public ColliderCount jumpCollider;
    
    private Rigidbody2D _rigidbody;
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;
    private static readonly int Walk = Animator.StringToHash("walk");
    private Vector2 _lastPos;

    private void Awake() {
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        _lastPos = transform.position;
        GameEvents.Instance.OnShipLandingFailed.AddListener(() => this.enabled = false);
    }

    void Start() {
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    void Update() {
        if (GameManager.Instance.isPaused)
            return;
        
        var deltaY = transform.position.y - _lastPos.y;
        _animator.SetFloat("deltaY", deltaY);
        
        bool isWalking = false;
        if (Input.GetKeyDown(KeyCode.Space) && jumpCollider.count > 0) {
            _rigidbody.AddForce(Vector2.up * jumpForce * (SpeedManager.Instance.gravity+1));
            _animator.SetTrigger("jumping");
        }
        
        var realAcceleration = acceleration * Time.deltaTime * SpeedManager.Instance.gravityForceMultiplier;
        if (Input.GetKey(KeyCode.A)) {
            _rigidbody.AddForce(Vector2.left * realAcceleration);
            isWalking = true;
            _spriteRenderer.flipX = true;
        }
        if (Input.GetKey(KeyCode.D)) {
            _rigidbody.AddForce(Vector2.right * realAcceleration);
            _spriteRenderer.flipX = false;
            isWalking = true;
        }
        if (Input.GetKey(KeyCode.W)) {
            _rigidbody.AddForce(Vector2.up * realAcceleration);
            isWalking = true;
        }
        if (Input.GetKey(KeyCode.S)) {
            _rigidbody.AddForce(Vector2.down * realAcceleration);
            isWalking = true;
        }
        _animator.SetBool(Walk, isWalking);
        _lastPos = transform.position;
    }
}
