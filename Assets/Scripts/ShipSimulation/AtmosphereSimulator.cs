using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtmosphereSimulator : MonoBehaviour {
    public Color start;
    public Color end;

    void Update() {
        Camera.main.backgroundColor = Color.Lerp(start, end, SpeedManager.Instance.percentDone);
    }
}
