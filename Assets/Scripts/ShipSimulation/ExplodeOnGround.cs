using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeOnGround : MonoBehaviour {
    public GameObject explosionPrefab;
    
    private void OnCollisionEnter2D(Collision2D other) {
        if (!other.gameObject.CompareTag("Ground"))
            return;
        
        GameEvents.Instance.OnShipLanded.Invoke();
        var sm = SpeedManager.Instance;
        if (Mathf.Abs(sm.shipSpeed.x) < sm.maxLandingSpeed.x
            && Mathf.Abs(sm.shipSpeed.y) < sm.maxLandingSpeed.y) {
            GameEvents.Instance.OnShipLandingSuccess.Invoke();
        }
        else {
            GameEvents.Instance.OnShipLandingFailed.Invoke();
            foreach (Transform child in explosionPrefab.transform) {
                child.eulerAngles = Vector3.zero;
            }
            explosionPrefab.GetComponent<Animator>().SetTrigger("explode");
        }
        ZoomOutCamera(other.GetContact(0).point);
    }

    private static void ZoomOutCamera(Vector2 groundTouchPoint) {
        var main = Camera.main;
        main.orthographicSize = 25;
        var pos = main.transform.position;
        pos.x = groundTouchPoint.x;
        pos.y = groundTouchPoint.y;
    }
}
