using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour {
    public float spawnInterval;
    public GameObject prefab;
    public Transform parentTo;
    
    private float _lastSpawn = Single.MinValue;

    // Update is called once per frame
    void Update() {
        if (Time.timeSinceLevelLoad - _lastSpawn > spawnInterval) {
            _lastSpawn = Time.timeSinceLevelLoad;
            var cameraY = Camera.main.transform.position.y;
            var position = new Vector3(0, -20 + cameraY, 0);
            var cloud = Instantiate(prefab, position, Quaternion.identity);
            cloud.transform.parent = parentTo;
        }
    }
}
