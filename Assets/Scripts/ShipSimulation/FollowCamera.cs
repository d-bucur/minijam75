using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {
    public bool followX;

    // Update is called once per frame
    void Update() {
        if (followX) {
            var pos = transform.position;
            pos.x = Camera.main.transform.position.x;
            transform.position = pos;
        }
    }
}
