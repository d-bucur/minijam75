using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarFieldGenerator : MonoBehaviour {
    public float forceMultiplier;
    public ParticleSystemForceField forceField;
    public ParticleSystem starGenerator;

    private void Awake() {
        GameEvents.Instance.OnShipLanded.AddListener(() => {
            forceField.gameObject.SetActive(false);
            starGenerator.gameObject.SetActive(false);
        });
    }

    private void LateUpdate() {
        var shipSpeed = SpeedManager.Instance.shipSpeed;
        forceField.directionX = -shipSpeed.x * forceMultiplier;
        forceField.directionY = -shipSpeed.y * forceMultiplier;
    }
}
