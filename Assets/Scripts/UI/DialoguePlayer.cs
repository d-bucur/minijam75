using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public enum Participant {
    Robot = 0,
    Data
}

[Serializable]
public struct DialogueLine {
    public Participant participant;
    public Sprite portrait;
    public string line;
}

public class DialoguePlayer : MonoBehaviour {
    public bool isEnabled = true;
    public List<DialogueLine> lines;
    public KeyCode continueKey;
    public Image portraitTarget;
    public TextMeshProUGUI textTarget;
    public TextMeshProUGUI instructionTarget;
    public GameObject uiRoot;

    private int currentItem = 0;
    private bool _isActive = false;

    private void Awake() {
        uiRoot.SetActive(false);
        DisplayItem(currentItem);
        instructionTarget.text = $"({continueKey}) to continue";
        GameEvents.Instance.OnIntroStarted.AddListener(Activate);
        }

    private void Start() {
        if (!isEnabled) {
            Stop();
        }
    }

    private void Update() {
        if (_isActive && Input.GetKeyDown(continueKey)) {
            currentItem++;
            if (currentItem == lines.Count) {
                Stop();
                return;
            }
            DisplayItem(currentItem);
        }
    }

    private void DisplayItem(int i) {
        var dialogue = lines[i];
        portraitTarget.sprite = dialogue.portrait;
        textTarget.text = dialogue.line;
    }

    public void Activate() {
        _isActive = true;
        uiRoot.SetActive(true);
    }

    private void Stop() {
        _isActive = false;
        uiRoot.SetActive(false);
        GameEvents.Instance.OnIntroFinished.Invoke();
    }
}
