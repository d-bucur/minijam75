using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedSlider : MonoBehaviour {
    public bool isX;
    
    void Start()
    {
        if (isX)
            GameEvents.Instance.OnSpeedXUpdate.AddListener(Handle);
        else
            GameEvents.Instance.OnSpeedYUpdate.AddListener(Handle);
    }

    private void Handle(string value) {
        var val = float.Parse(value);
        var scale = transform.localScale;
        if (isX)
            scale.x = val;
        else
            scale.y = -val;
        transform.localScale = scale;

        
        float max = isX ? SpeedManager.Instance.maxLandingSpeed.x : SpeedManager.Instance.maxLandingSpeed.y;
        float absVal = Mathf.Abs(val);
        var color = Color.Lerp(Color.green, Color.yellow, absVal / max);
        if (absVal > max)
            color = Color.red;
        GetComponent<Image>().color = color;
    }
}
