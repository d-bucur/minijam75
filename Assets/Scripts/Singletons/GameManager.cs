using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public bool isPaused { get; private set; }
    public float timeToRestart;

    void Awake() {
        Instance = this;
        GameEvents.Instance.OnIntroFinished.AddListener(Unpause);
        GameEvents.Instance.OnIntroStarted.AddListener(Pause);
        GameEvents.Instance.OnShipLandingFailed.AddListener(() => LoadSceneDelayed(SceneManager.GetActiveScene().buildIndex, timeToRestart));
        GameEvents.Instance.OnShipLandingSuccess.AddListener(() => LoadSceneDelayed(2, 1));
    }

    public void LoadSceneDelayed(int sceneIdx, float delay) {
        StartCoroutine(LoadSceneDelayedCoroutine(sceneIdx, delay));
    }

    private IEnumerator LoadSceneDelayedCoroutine(int sceneIdx, float delay = 0f) {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(sceneIdx, LoadSceneMode.Single);
    }

    void Start() {
        GameEvents.Instance.OnIntroStarted.Invoke();
        Application.targetFrameRate = 60;
    }

    private void Unpause() {
        isPaused = false;
        Time.timeScale = 1f;
    }

    private void Pause() {
        isPaused = true;
        Time.timeScale = 0f;
    }
}
