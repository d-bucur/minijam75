using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEvents : MonoBehaviour {
    public static GameEvents Instance;
    public UnityEvent OnIntroStarted;
    public UnityEvent OnIntroFinished;
    public UnityEvent<string> OnSpeedXUpdate;
    public UnityEvent<string> OnSpeedYUpdate;
    public UnityEvent<string> OnGravityUpdate;
    public UnityEvent<string> OnDistanceUpdate;
    public UnityEvent<string> OnTTIUpdate;
    public UnityEvent OnShipLanded;
    public UnityEvent OnShipLandingSuccess;
    public UnityEvent OnShipLandingFailed;

    void Awake() {
        Instance = this;
    }
}
