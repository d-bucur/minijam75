using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SpeedManager : MonoBehaviour {
    public static SpeedManager Instance;
    public Vector2 shipSpeed;
    [SerializeField] private float shipGravity;
    public bool updateGravity;
    public float gravity = 0f;
    public AnimationCurve gravityCurve;
    public Transform translatable;
    public Transform finish;
    public Vector2 maxLandingSpeed;


    public float percentDone { get; private set; }
    public float gravityForceMultiplier => gravity / 2f + 1;

    private float _totalDistance;
    private Camera _camera;

    void Awake() {
        Instance = this;
        _camera = Camera.main;
        _totalDistance = _camera.transform.position.y - finish.position.y;
        GameEvents.Instance.OnShipLanded.AddListener(() => gameObject.SetActive(false));
    }

    void Update() {
        percentDone = 1 - (_camera.transform.position.y - finish.position.y) / _totalDistance;
        if (updateGravity)
            gravity = gravityCurve.Evaluate(percentDone);
        shipSpeed.y -= shipGravity * Time.deltaTime;
        translatable.position -= (Vector3)shipSpeed * Time.deltaTime;
    }

    private void LateUpdate() {
        GameEvents.Instance.OnSpeedXUpdate.Invoke($"{shipSpeed.x:0}");
        GameEvents.Instance.OnSpeedYUpdate.Invoke($"{-shipSpeed.y:0}");
        GameEvents.Instance.OnGravityUpdate.Invoke($"{gravity:0.0}");
        var distanceRemaining = _camera.transform.position.y - finish.position.y;
        GameEvents.Instance.OnDistanceUpdate.Invoke($"{distanceRemaining:0}");
        var timeToImpact = distanceRemaining / shipSpeed.y;
        GameEvents.Instance.OnTTIUpdate.Invoke($"{-timeToImpact:0}");
    }

    private float EstimateTimeToImpact() {
        var distanceRemaining = _camera.transform.position.y - finish.position.y;
        return distanceRemaining / shipSpeed.y;
    }
}
