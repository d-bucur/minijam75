using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "LevelLoader", menuName = "ScriptableObjects/LevelLoader", order = 0)]
public class LevelLoader : ScriptableObject {
    public void LoadScene(int sceneIdx) {
        SceneManager.LoadScene(sceneIdx);
    }
}