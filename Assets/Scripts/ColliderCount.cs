using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderCount : MonoBehaviour {
    public int count = 0;
    private void OnTriggerEnter2D(Collider2D other) {
        if (other.isTrigger)
            return;
        count++;
    }
    private void OnTriggerExit2D(Collider2D other) {
        if (other.isTrigger)
            return;
        count--;
    }
}
