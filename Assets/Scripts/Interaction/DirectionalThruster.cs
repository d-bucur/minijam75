using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalThruster : Thruster {
    public float force;
    public Vector2 direction;

    private void Update() {
        if (!_activated)
            return;
        var shipDirection3 = transform.parent.up;
        var shipDirection = new Vector2(shipDirection3.x, shipDirection3.y);
        SpeedManager.Instance.shipSpeed += shipDirection * force * Time.deltaTime;
        
        // transform.parent.GetComponent<Rigidbody2D>().AddRelativeForce(force * direction);
    }
}
