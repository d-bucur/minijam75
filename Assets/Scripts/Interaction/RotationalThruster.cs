using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationalThruster : Thruster {
    public float torque;
    

    private void Update() {
        if (_activated) {
            transform.parent.GetComponent<Rigidbody2D>().AddTorque(torque * Time.deltaTime);
        }
    }
}
