using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {
    private int insideCounter = 0;
    
    private void OnTriggerEnter2D(Collider2D other) {
        if (!other.GetComponentInParent<Interactor>()) return;
        if (other.isTrigger) return;
        if (insideCounter == 0)
            GetComponent<Thruster>().Activate();
        insideCounter++;
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (!other.GetComponentInParent<Interactor>()) return;
        if (other.isTrigger) return;
        insideCounter--;
        if (insideCounter == 0)
            GetComponent<Thruster>().Deactivate();
    }
}
