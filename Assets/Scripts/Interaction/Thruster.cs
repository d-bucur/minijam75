using UnityEngine;

public abstract class Thruster : MonoBehaviour {
    protected bool _activated;
    private Animator _animator;

    private void Awake() {
        _animator = GetComponentInChildren<Animator>();
    }

    public void Activate() {
        _activated = true;
        _animator.SetBool("on", true);
    }

    public void Deactivate() {
        _activated = false;
        _animator.SetBool("on", false);
    }
}