using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct SwitchBinder {
    public KeyCode key;
    public Thruster thruster;
}

public class Cheats : MonoBehaviour {
    public List<SwitchBinder> thrusters;

    // Update is called once per frame
    void Update()
    {
        foreach (var t in thrusters) {
            if (Input.GetKeyDown(t.key)) {
                t.thruster.Activate();
            }
            if (Input.GetKeyUp(t.key)) {
                t.thruster.Deactivate();
            }
        }
    }
}
