using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour {
    [TextArea]
    public string text;
    public TextMeshProUGUI textTarget;
    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(UpdateText);
    }

    private void UpdateText() {
        textTarget.text = text;
    }
}
