using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    private void Awake() {
        gameObject.SetActive(false);
        GameEvents.Instance.OnIntroFinished.AddListener(() => gameObject.SetActive(true));
    }
}
