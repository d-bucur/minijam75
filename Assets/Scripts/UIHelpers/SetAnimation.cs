using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimation : MonoBehaviour {
    public string triggerName;
    void Start()
    {
        GetComponent<Animator>().SetTrigger(triggerName);
    }
}
